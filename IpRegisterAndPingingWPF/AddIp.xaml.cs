﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IpRegisterAndPingingWPF
{
    /// <summary>
    /// AddIp.xaml etkileşim mantığı
    /// </summary>
    public partial class AddIp : Window
    {
        private string selectedList;
        public AddIp()
        {
            InitializeComponent();
            areasSelectionList();
            areasSelect.SelectedIndex = 0;
            selectedList = areasSelect.SelectedItem.ToString();
        }
        private void areasSelectionList()
        {
            string startupPath = System.IO.Directory.GetParent(@"./").FullName;
            string path1 = startupPath;
            Directory.CreateDirectory(@startupPath + "/IpList");
            DirectoryInfo dinfo = new DirectoryInfo(@startupPath + "/IpList");
            FileInfo[] Files = dinfo.GetFiles("*.txt");
            foreach (FileInfo file in Files)
            {
                areasSelect.Items.Add(file.Name);
            }
        }

        private void addNewDevice_Click(object sender, RoutedEventArgs e)
        {
            Boolean bValid = IsValidIPAddress(ipAdressInput.Text);
            if (ipAdressInput.Text != "" && deviceNameInput.Text !="" && selectedList != "")
            {
                if (bValid == true)
                {
                    string startupPath = System.IO.Directory.GetParent(@"./").FullName;
                    using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(startupPath + "/IpList/" + selectedList, true))
                    {
                        file.WriteLine("\n" + ipAdressInput.Text + "-" + deviceNameInput.Text);
                    }
                    ((MainWindow)this.Owner).newDataAction(selectedList);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Hatalı ip adresi girdiniz.", "Hata!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                
            }
            else
            {
                MessageBox.Show("Tüm alanları eksiksiz bir şekilde doludurun.","Hata!",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            
            
        }
        public bool IsValidIPAddress(string addr)
        {
            IPAddress address;
            return IPAddress.TryParse(addr, out address);
        }
        private void areasSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedList = areasSelect.SelectedItem.ToString();
        }
    }
}
