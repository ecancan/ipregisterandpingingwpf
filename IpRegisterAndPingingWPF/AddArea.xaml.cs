﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IpRegisterAndPingingWPF
{
    /// <summary>
    /// AddArea.xaml etkileşim mantığı
    /// </summary>
    public partial class AddArea : Window
    {
        public AddArea()
        {
            InitializeComponent();
        }

        private void addNewArea_Click(object sender, RoutedEventArgs e)
        {
            if (areaName.Text != "")
            {
                string startupPath = System.IO.Directory.GetParent(@"./").FullName;
                string file = System.IO.Path.Combine(startupPath + "/IpList/", areaName.Text + ".txt");
                File.WriteAllText(file, "");
                ((MainWindow)this.Owner).addNewArea(areaName.Text + ".txt");
                this.Close();
            }
            else
            {
                MessageBox.Show("Eksik alan bırakmayın!", "Hata!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
    }
}
