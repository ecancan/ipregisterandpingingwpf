﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IpRegisterAndPingingWPF
{
    class IpDataList
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Ip { get; set; }
        public String Status { get; set; }
        
        public static  ObservableCollection<IpDataList> getIpDataList(string filePath)
        {
            string startupPath = System.IO.Directory.GetParent(@"./").FullName;
            var ipDataList = new ObservableCollection<IpDataList>();
            string[] lines = File.ReadAllLines(startupPath+"/IpList/"+ filePath, Encoding.UTF8).ToArray();
            int counter = 1;
            foreach (var line in lines)
            {
                
                if (line.Trim() != string.Empty)
                {
                    counter++;
                    string[] datas = line.Split('-');
                    Console.WriteLine(datas[1]);
                    Ping p = new Ping();
                    PingReply r;
                    string s;
                    s = datas[0];
                    r = p.Send(s);

                    if (r.Status == IPStatus.Success)
                    {
                        ipDataList.Add(new IpDataList() { Id = counter - 1, Name = datas[1], Ip = datas[0], Status = "Aktif" });
                        //lblResult.Text = "Ping to " + s.ToString() + "[" + r.Address.ToString() + "]" + " Successful"+ " Response delay = " + r.RoundtripTime.ToString() + " ms" + "\n";
                    }
                    else
                    {
                        ipDataList.Add(new IpDataList() { Id = counter - 1, Name = datas[1], Ip = datas[0], Status = "Pasif" });
                    }
                }

            }
            return ipDataList;
        }

    }
}
