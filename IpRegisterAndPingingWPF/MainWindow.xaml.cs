﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Collections.ObjectModel;
using System.Threading;
using System.Net.NetworkInformation;
using System.Net;
using System.ComponentModel;

namespace IpRegisterAndPingingWPF
{
    /// <summary>
    /// MainWindow.xaml etkileşim mantığı
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<IpDataList> ipListData;
        private string selectedList;
        public MainWindow()
        {
            InitializeComponent();
            areasSelectionList();
            areasSelect.SelectedIndex = 0;
            try
            {
                selectedList = areasSelect.SelectedItem.ToString();
                areasSelect.IsEnabled = false;
                dataGridView.ItemsSource = "";
                progressItem.Visibility = Visibility.Visible;
                addIp.IsEnabled = false;
                refreshButton.IsEnabled = false;
                deleteAllButton.IsEnabled = false;
                searchText.IsEnabled = false;
                statusSelect.IsEnabled = false;
            }
            catch (Exception)
            {
                selectedList = "";
                dataGridView.ItemsSource = "";
                progressItem.Visibility = Visibility.Hidden;
            }
            
            areasSelect.IsEnabled = false;
            addIp.IsEnabled = false;
            refreshButton.IsEnabled = false;
            deleteAllButton.IsEnabled = false;
            searchText.IsEnabled = false;
            statusSelect.IsEnabled = false;

            IPHostEntry IPHost = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            if (IPHost != null)
            {
                foreach (var ipAddress in IPHost.AddressList)
                {
                    ipAdressLabel.Content = "IP Adresiniz : "+ipAddress.ToString();

                }
            }


            statusSelect.Items.Add("");
            statusSelect.Items.Add("Aktif");
            statusSelect.Items.Add("Pasif");
            statusSelect.SelectedIndex = 0;
        }
        private void aboutProgram_Click(Object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.Owner = this;
            about.ShowDialog();

        }
        private void dataButton_Click(Object sender,RoutedEventArgs e)
        {
            addIp.IsEnabled = false;
            areasSelect.IsEnabled = false;
            deleteAllButton.IsEnabled = false;
            searchText.IsEnabled = false;
            statusSelect.IsEnabled = false;
            IpDataList ipList = dataGridView.SelectedItem as IpDataList;
            //Console.WriteLine(dataGridView.SelectedItem);
            string ipId = ipList.Id.ToString();
            string deviceName = ipList.Name;
            string deviceIp = ipList.Ip;
            progressItem.Visibility = Visibility.Visible;
            refreshButton.IsEnabled = false;
            Thread thread = new Thread(() => CheckIp(ipId, deviceName, deviceIp));
            thread.Start();
            
        }
        private void deleteButton_Click(Object sender, RoutedEventArgs e)
        {
            addIp.IsEnabled = false;
            areasSelect.IsEnabled = false;
            searchText.IsEnabled = false;
            statusSelect.IsEnabled = false;
            IpDataList ipList = dataGridView.SelectedItem as IpDataList;
            //Console.WriteLine(dataGridView.SelectedItem);
            string ipId = ipList.Id.ToString();
            string deviceName = ipList.Name;
            string deviceIp = ipList.Ip;
            progressItem.Visibility = Visibility.Visible;
            refreshButton.IsEnabled = false;
            deleteAllButton.IsEnabled = false;
            string startupPath = System.IO.Directory.GetParent(@"./").FullName;
            var ipDataList = new ObservableCollection<IpDataList>();
            string[] lines = File.ReadAllLines(startupPath + "/IpList/" + selectedList, Encoding.UTF8).ToArray();
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(startupPath + "/IpList/" + selectedList))
            {
                foreach (string line in lines)
                {
                    if (!line.Contains(deviceName))
                    {
                        file.WriteLine(line);
                    }
                }
            }
            
            Thread thread = new Thread(() => addIpList(selectedList));
            thread.Start();

        }
        private void addIp_Click(object sender, RoutedEventArgs e)
        {

            AddIp addIpWindow = new AddIp();
            addIpWindow.Owner = this;
            addIpWindow.ShowDialog();

        }
        private void addArea_Click(object sender, RoutedEventArgs e)
        {
            
            AddArea addAreaWindow = new AddArea();
            addAreaWindow.Owner = this;
            addAreaWindow.ShowDialog();
            
        }
        private void dataRefresh(object sender, RoutedEventArgs e)
        {
            addIp.IsEnabled = false;
            areasSelect.IsEnabled = false;
            deleteAllButton.IsEnabled = false;
            searchText.IsEnabled = false;
            statusSelect.IsEnabled = false;
            progressItem.Visibility = Visibility.Visible;
            Thread thread = new Thread(() => addIpList(selectedList));
            thread.Start();


        }
        public void addNewArea(string itemName)
        {
            areasSelect.Items.Add(itemName);
        }
        public void newDataAction(string selectedFromNewWindow)
        {
            addIp.IsEnabled = false;
            areasSelect.SelectedItem = selectedFromNewWindow;
            areasSelect.IsEnabled = false;
            refreshButton.IsEnabled = false;
            deleteAllButton.IsEnabled = false;
            searchText.IsEnabled = false;
            statusSelect.IsEnabled = false;
            progressItem.Visibility = Visibility.Visible;
            Thread thread = new Thread(() => addIpList(selectedFromNewWindow));
            thread.Start();


        }
        private void areasSelectionList()
        {
            string startupPath = System.IO.Directory.GetParent(@"./").FullName;
            Directory.CreateDirectory(@startupPath + "/IpList");
            DirectoryInfo dinfo = new DirectoryInfo(@startupPath + "/IpList");
            FileInfo[] Files = dinfo.GetFiles("*.txt");
            foreach (FileInfo file in Files)
            {
                areasSelect.Items.Add(file.Name);
            }
        }
        private int areasSelectionListCount()
        {
            string startupPath = System.IO.Directory.GetParent(@"./").FullName;
            Directory.CreateDirectory(@startupPath + "/IpList");
            DirectoryInfo dinfo = new DirectoryInfo(@startupPath + "/IpList");
            FileInfo[] Files = dinfo.GetFiles("*.txt");
            int counter = 0;
            foreach (FileInfo file in Files)
            {
                counter++;
            }
            return counter;
        }
        private void areasSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                dataGridView.ItemsSource = "";
                areasSelect.IsEnabled = false;
                refreshButton.IsEnabled = false;
                deleteAllButton.IsEnabled = false;
                searchText.IsEnabled = false;
                statusSelect.IsEnabled = false;
                selectedList = areasSelect.SelectedItem.ToString();
                progressItem.Visibility = Visibility.Visible;
                if (selectedList != "")
                {
                    Thread thread = new Thread(() => addIpList(selectedList));
                    thread.Start();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("İlgili dosya silnmiştir. Tekrar yukarıdan seçim yapınız.", "Uyarı!", MessageBoxButton.OK, MessageBoxImage.Information);
                areasSelect.IsEnabled = true;
            }


        }
        
        public void addIpList(string selectedList)
        {
            ipListData = IpDataList.getIpDataList(selectedList);
            Dispatcher.BeginInvoke(new Action(() =>
            {
                dataGridView.ItemsSource = ipListData;
                progressItem.Visibility = Visibility.Hidden;
                refreshButton.IsEnabled = true;
                areasSelect.IsEnabled = true;
                addIp.IsEnabled = true;
                deleteAllButton.IsEnabled = true;
                searchText.IsEnabled = true;
                statusSelect.IsEnabled = true;
            }));
        }
        public void CheckIp(string ipId,string deviceName,string deviceIp)
        {
            Ping p = new Ping();
            PingReply r;
            string s;
            
            s = deviceIp;
            r = p.Send(s);
            if (r.Status == IPStatus.Success)
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    IpDataList ipList = dataGridView.SelectedItem as IpDataList;
                    progressItem.Visibility = Visibility.Hidden;
                    ipList.Status = "Aktif";
                    dataGridView.Items.Refresh();
                    MessageBox.Show(s + " --> " + r.RoundtripTime.ToString() + " ms Yanıt Süresi", "Uyarı!", MessageBoxButton.OK, MessageBoxImage.Information);
                    refreshButton.IsEnabled = true;
                    areasSelect.IsEnabled = true;
                    deleteAllButton.IsEnabled = true;
                    addIp.IsEnabled = true;
                    searchText.IsEnabled = true;
                    statusSelect.IsEnabled = true;
                }));
            }
            else
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    IpDataList ipList = dataGridView.SelectedItem as IpDataList;
                    progressItem.Visibility = Visibility.Hidden;
                    ipList.Status = "Pasif";
                    dataGridView.Items.Refresh();
                    MessageBox.Show(s + " --> Yanıt Alınamadı", "Hata!", MessageBoxButton.OK, MessageBoxImage.Error);
                    refreshButton.IsEnabled = true;
                    areasSelect.IsEnabled = true;
                    addIp.IsEnabled = true;
                    deleteAllButton.IsEnabled = true;
                    searchText.IsEnabled = true;
                    statusSelect.IsEnabled = true;

                }));
            }
        }

        private void deleteAllButton_Click(object sender, RoutedEventArgs e)
        {
            int fileCount = areasSelectionListCount();
            Console.WriteLine(fileCount);
            if (fileCount <= 1)
            {
                MessageBox.Show("En az bir bölge kalmak zorundadır.", "Hata!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                string startupPath = System.IO.Directory.GetParent(@"./").FullName;
                System.IO.File.Delete(startupPath + "/IpList/" + selectedList);
                areasSelect.Items.RemoveAt(areasSelect.SelectedIndex);
                Console.WriteLine(areasSelect.SelectedIndex);
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            statusSelect.SelectedIndex = 0;
            var itemSourceList = new CollectionViewSource() { Source = ipListData };
            ICollectionView Itemlist = itemSourceList.View;
            var nameFilter = new Predicate<object>(item => ((IpDataList)item).Name.Contains(searchText.Text));
            Itemlist.Filter = nameFilter;
            dataGridView.ItemsSource = Itemlist;
        }

        private void statusSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            //Boş
        }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            searchText.Text = "";
            try
            {
                var itemSourceList = new CollectionViewSource() { Source = ipListData };
                ICollectionView Itemlist = itemSourceList.View;
                var StatusFilter = new Predicate<object>(item => ((IpDataList)item).Status.Equals(statusSelect.Text));
                Itemlist.Filter = StatusFilter;
                dataGridView.ItemsSource = Itemlist;
                Console.WriteLine(statusSelect.Text);
            }
            catch (Exception)
            {
                // boş 
            }
        }
    }
}
